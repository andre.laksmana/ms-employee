# ms-employee

This is the seed for Node.js with Hapi.js. This should be _forked_ to create any microservice.

## Environment variables to be passed in pipeline

```
SQL_PASSWORD

MONGO_PASSWORD
```

## Version specification
- node: v10.16.0
- TypeScript v3.5

## IDE recommendations and setup

- VSCode IDE
- Eslint vscode plugin
- Prettier vscode plugin for linting warnings (auto fix on save)
- Add the following setting in vscode settings.json 
```json
"eslint.autoFixOnSave": true
```

## Dev setup
- Install all the dependencies using `npm install`
- To run the server with watch use `npm run dev-watch`
- To run the test cases in watch mode use `npm run test-watch`
- To run the test cases without watch mode use `npm run test`
- To run docker compose `docker-compose -f docker-compose.dev.yml up -d`
- To build docker image only :
`
 npm tsc &&
 docker build -t ${your image name} .
`
## Run the API with a specific environment

Follow document https://www.npmjs.com/package/nconf please check the environment under configs folder . Default NODE_ENV is local environment (default.json)

  ```bash
    NODE_ENV=dev npm run dev:watch
  ```

## Test

- Unit Test: We are using Jest for assertion and mocking

## Git Hooks
The seed uses `husky` to enable commit hook.

### Pre commit
Whenever there is a commit, there will be check on lint, on failure commit fails.

### Pre push
Whenever there is a push, there will be check on test.

## Internal Module dependencies
The following are module dependecies:
- module-logger: for all the logging
- module-http: for making http calls

## ENV variables

### Optional
- LOG_FILE: path to log file, default console log

## Working with TypeORM migration script

Migration scritps are developed in TypeScript and located at `migrations` folder.

Steps to create migration script

Create migration script
```bash
npx typeorm migration:create -n <Migration class name>
```

Run migration script
```bash
host=<db-host> port=<db-port> username=<username> password=<pwd> npm run migrate
```

## Working with Mongo migration script

**Limitation**: can not reuse model from typescript because migration scripts are not supported in in **Typescript** yet

Steps to create migration script

Create migration script
```bash
npm run migrate:create <Migration class name>
```

Run migration script up
```bash
npm run migrate:up
```

Run migration script down
```bash
npm run migrate:down
```
## Deployment CI/CD
* Find `RENAME_THIS_TO_DEVELOP` and replace to `develop`
* Find `RENAME_THIS_TO_SERVICE_NAME` and replace to `the name of the new service`

## Misc

Swagger API is at http://localhost:8080/documentation
