"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const hapi_1 = __importDefault(require("@hapi/hapi"));
const Inert = __importStar(require("@hapi/inert"));
const Vision = __importStar(require("@hapi/vision"));
const cls_hooked_1 = require("cls-hooked");
const mongoDb_1 = require("./common/mongoDb");
const handleValidationErrors_1 = __importDefault(require("./common/handleValidationErrors"));
// import sqlDb from './common/sqlDb'; // Commenting for now will revert
const swagger_1 = __importDefault(require("./plugins/swagger"));
const good_1 = __importDefault(require("./plugins/good"));
const responseWrapper_1 = __importDefault(require("./plugins/responseWrapper"));
const requestWrapper_1 = __importDefault(require("./plugins/requestWrapper"));
const constant_1 = require("./common/constant");
const routes_1 = require("./routes");
const logger_1 = __importDefault(require("./logger"));
const config_1 = require("./config");
const { port, host } = config_1.config.get('server');
const createServer = () => __awaiter(void 0, void 0, void 0, function* () {
    const server = new hapi_1.default.Server({
        port,
        host,
        routes: {
            validate: {
                options: {
                    abortEarly: false
                },
                failAction: handleValidationErrors_1.default
            }
        }
    });
    // Register routes
    server.route(routes_1.routes);
    server.events.on('response', request => {
        const session = cls_hooked_1.getNamespace(constant_1.Tracing.TRACER_SESSION);
        // @ts-ignore suggest any better approach?
        const clsCtx = request.app[constant_1.Tracing.TRACER_SESSION].context;
        session.exit(clsCtx);
    });
    const plugins = [
        Inert,
        Vision,
        swagger_1.default,
        good_1.default,
        responseWrapper_1.default,
        requestWrapper_1.default
    ];
    yield server.register(plugins);
    return server;
});
exports.init = () => __awaiter(void 0, void 0, void 0, function* () {
    yield mongoDb_1.connectMongo();
    const server = yield createServer();
    yield server
        .initialize()
        .then(() => logger_1.default.info(`server started at ${server.info.host}:${server.info.port}`));
    return server;
});
exports.start = (module) => __awaiter(void 0, void 0, void 0, function* () {
    if (!module.parent) {
        // Commenting for now will revert
        // await sqlDb.init();
        logger_1.default.info('Start server');
        yield exports.init()
            .then(server => server.start())
            .catch(err => {
            logger_1.default.error('Server cannot start', err);
            logger_1.default.onFinished(() => {
                process.exit(1);
            });
        });
    }
});
exports.start(module);
//# sourceMappingURL=server.js.map