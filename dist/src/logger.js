"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_logger_1 = require("@dk/module-logger");
const constant_1 = require("../src/common/constant");
const logger = module_logger_1.createLogger({
    defaultMeta: {
        service: 'seed-ms'
    },
    tracing: {
        tracerSessionName: constant_1.Tracing.TRACER_SESSION,
        requestId: constant_1.Tracing.TRANSACTION_ID
    }
});
exports.default = logger;
//# sourceMappingURL=logger.js.map