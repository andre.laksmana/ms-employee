"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const healthcheck_controller_1 = __importDefault(require("./healthcheck/healthcheck.controller"));
const employee_controller_1 = __importDefault(require("./employee/employee.controller"));
const routes = [...healthcheck_controller_1.default, ...employee_controller_1.default];
exports.routes = routes;
//# sourceMappingURL=routes.js.map