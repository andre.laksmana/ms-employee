"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cls_hooked_1 = require("cls-hooked");
const constant_1 = require("../common/constant");
afterEach(() => {
    expect.hasAssertions();
});
const session = constant_1.Tracing.TRACER_SESSION;
beforeAll(() => {
    cls_hooked_1.createNamespace(session);
});
afterAll(() => {
    try {
        cls_hooked_1.destroyNamespace(session);
    }
    catch (_a) { }
});
//# sourceMappingURL=setup.js.map