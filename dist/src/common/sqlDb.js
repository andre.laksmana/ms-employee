"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* istanbul ignore file */ // TODO this will be removed. Jest got a bug w.r.t coverage on some scearios like object destructuring
const typeorm_1 = require("typeorm");
const entities = __importStar(require("../entities"));
const logger_1 = __importDefault(require("../logger"));
const config_1 = require("../config");
const constant_1 = require("./constant");
const init = () => __awaiter(void 0, void 0, void 0, function* () {
    const db = config_1.config.get(constant_1.ConfigParameter.db);
    const { host, port, username, database, type, synchronize } = db;
    logger_1.default.info('Connect SQL server');
    return yield typeorm_1.createConnection({
        host,
        port,
        type,
        database,
        username,
        password: process.env.SQL_PASSWORD,
        synchronize,
        entities: Object.values(entities)
    });
});
const SqlDb = {
    init
};
exports.default = SqlDb;
//# sourceMappingURL=sqlDb.js.map