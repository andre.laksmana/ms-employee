"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("../common/errors");
const AppError_1 = require("../errors/AppError");
const errorHandler = (_req, res, err) => {
    if (!err) {
        return res.continue;
    }
    if (err.isJoi) {
        const details = err.details;
        const mappedDetails = details.reduce((acc, detail, index) => {
            if (index !== 0 && detail.path[0] === details[index - 1].path[0]) {
                return acc;
            }
            const constraint = detail.type.split('.').pop() || '';
            const errorCode = errors_1.JoiValidationErrors[constraint] ||
                errors_1.ERROR_CODE.INCORRECT_FIELD;
            const defaultError = errors_1.ErrorList[errorCode];
            acc.push({
                message: defaultError.message,
                code: errorCode,
                key: detail.path[0]
            });
            return acc;
        }, []);
        throw new AppError_1.AppError(errors_1.ERROR_CODE.INVALID_REQUEST, mappedDetails);
    }
    throw err;
};
exports.default = errorHandler;
//# sourceMappingURL=handleValidationErrors.js.map