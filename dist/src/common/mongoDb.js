"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const module_config_1 = require("@dk/module-config");
const logger_1 = __importDefault(require("../logger"));
const module_common_1 = require("@dk/module-common");
// TODO: use object to wrap mongo configuration in env
exports.MONGO_USER_PARAM = 'MONGO_USER';
exports.MONGO_PASS_PARAM = 'MONGO_PASS';
exports.MONGO_DB_NAME = 'MONGO_DB_NAME';
exports.MONGO_HOSTS = 'MONGO_HOSTS';
exports.MONGO_RECONNECT_RETRIES = 'MONGO_RECONNECT_RETRIES';
exports.MONGO_RECONNECT_INTERVAL = 'MONGO_RECONNECT_INTERVAL';
exports.connectMongo = () => new Promise((resolve, reject) => {
    const user = module_config_1.config.get(exports.MONGO_USER_PARAM);
    const pass = process.env.MONGO_PASSWORD || '';
    const dbName = module_config_1.config.get(exports.MONGO_DB_NAME);
    const dbHosts = module_config_1.config.get(exports.MONGO_HOSTS);
    const reconnectInterval = Number(module_config_1.config.get(exports.MONGO_RECONNECT_INTERVAL)) || 0;
    const reconnectTries = Number(module_config_1.config.get(exports.MONGO_RECONNECT_RETRIES)) || 3;
    const autoReconnect = reconnectInterval > 0;
    let dbUri = '';
    if (user) {
        dbUri = module_common_1.Util.getMongoUri(user, pass, dbName, dbHosts);
    }
    else {
        dbUri = 'mongodb://' + dbHosts + '/' + dbName;
    }
    // const dbUri =
    //   'mongodb+srv://andre:spLJkFUF7SlOWjv4@cluster0-ufejz.mongodb.net/test?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=true';
    if (!dbName) {
        logger_1.default.error(`No ${exports.MONGO_DB_NAME} is provided`);
        return reject(`No ${exports.MONGO_DB_NAME} is provided`);
    }
    mongoose_1.connection.on('error', (err) => {
        logger_1.default.error('error while connecting to mongodb', err);
    });
    if (!autoReconnect) {
        mongoose_1.connection.once('error', reject); // reject first error
    }
    mongoose_1.connection.once('open', () => {
        if (!autoReconnect) {
            mongoose_1.connection.off('error', reject);
        }
        resolve();
    });
    mongoose_1.connection.on('reconnected', () => {
        logger_1.default.info('Connection to mongodb is resumed');
    });
    mongoose_1.connection.on('disconnected', () => {
        logger_1.default.error('Mongodb disconnected');
    });
    mongoose_1.connect(dbUri, {
        useNewUrlParser: true,
        user,
        pass,
        dbName,
        autoReconnect,
        reconnectTries,
        reconnectInterval
    });
});
//# sourceMappingURL=mongoDb.js.map