"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
var ERROR_CODE;
(function (ERROR_CODE) {
    ERROR_CODE["USER_AGE_NOT_VALID"] = "USER_AGE_NOT_VALID";
    ERROR_CODE["USER_NAME_EXISTED"] = "USER_NAME_EXISTED";
    ERROR_CODE["USER_NAME_NOT_EXISTED"] = "USER_NAME_NOT_EXISTED";
    ERROR_CODE["INCORRECT_EMAIL_FORMAT"] = "INCORRECT_EMAIL_FORMAT";
    ERROR_CODE["INCORRECT_FIELD"] = "INCORRECT_FIELD";
    ERROR_CODE["INVALID_REQUEST"] = "INVALID_REQUEST";
    ERROR_CODE["UNEXPECTED_ERROR"] = "UNEXPECTED_ERROR"; // do not use this when create AppError
})(ERROR_CODE || (ERROR_CODE = {}));
exports.ERROR_CODE = ERROR_CODE;
const JoiValidationErrors = {
    email: ERROR_CODE.INCORRECT_EMAIL_FORMAT
};
exports.JoiValidationErrors = JoiValidationErrors;
const ErrorList = {
    [ERROR_CODE.USER_AGE_NOT_VALID]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'sth'
    },
    [ERROR_CODE.USER_NAME_EXISTED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: "User's name already existed"
    },
    [ERROR_CODE.USER_NAME_NOT_EXISTED]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: "User's name does not exist"
    },
    [ERROR_CODE.INCORRECT_EMAIL_FORMAT]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Incorrect email format'
    },
    [ERROR_CODE.INCORRECT_FIELD]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Incorrect field value, data type or length'
    },
    [ERROR_CODE.INVALID_REQUEST]: {
        statusCode: module_common_1.Http.StatusCode.BAD_REQUEST,
        message: 'Invalid request'
    },
    [ERROR_CODE.UNEXPECTED_ERROR]: {
        statusCode: module_common_1.Http.StatusCode.INTERNAL_SERVER_ERROR,
        message: 'We caught unexpected error'
    }
};
exports.ErrorList = ErrorList;
//# sourceMappingURL=errors.js.map