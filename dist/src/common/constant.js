"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ConfigParameter = {
    db: 'db'
};
exports.ConfigParameter = ConfigParameter;
const Tracing = {
    TRACER_SESSION: 'TRACER_SESSION',
    TRANSACTION_ID: 'x-request-id'
};
exports.Tracing = Tracing;
//# sourceMappingURL=constant.js.map