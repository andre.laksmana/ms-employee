"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ping = {
    method: 'GET',
    path: '/ping',
    options: {
        description: 'Pongs back',
        notes: 'To check is service pongs on a ping',
        tags: ['api'],
        handler: (_request, _h) => {
            return 'pong!';
        }
    }
};
const healthController = [ping];
exports.default = healthController;
//# sourceMappingURL=healthcheck.controller.js.map