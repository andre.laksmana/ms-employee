"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Joi = __importStar(require("@hapi/joi"));
const user_constant_1 = __importDefault(require("./user.constant"));
const validators_1 = require("../common/validators");
const UserValidator = {
    name: Joi.string()
        .trim()
        .required(),
    age: Joi.number()
        .min(user_constant_1.default.MIN_USER_AGE)
        .required(),
    mobile: Joi.string().trim()
};
exports.UserValidator = UserValidator;
const UserResponseValidator = Joi.object(Object.assign(Object.assign({}, validators_1.MongooseBase), UserValidator))
    .required()
    .label('Response - User');
exports.UserResponseValidator = UserResponseValidator;
const UserListResponseValidator = Joi.array()
    .items(UserResponseValidator)
    .label('Response - Users');
exports.UserListResponseValidator = UserListResponseValidator;
const createUserRequestValidator = Joi.object(Object.assign({}, UserValidator)).label('Request - new user');
exports.createUserRequestValidator = createUserRequestValidator;
//# sourceMappingURL=user.validator.js.map