"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const module_common_1 = require("@dk/module-common");
const user_service_1 = __importDefault(require("./user.service"));
const user_validator_1 = require("./user.validator");
const getUser = {
    method: module_common_1.Http.Method.GET,
    path: '/users',
    options: {
        description: 'Get the list of users',
        notes: 'Get the list of users',
        tags: ['api', 'users'],
        response: {
            schema: user_validator_1.UserListResponseValidator
        },
        handler: () => {
            return user_service_1.default.getUsers();
        },
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.OK]: {
                        description: 'Users retrieved'
                    }
                }
            }
        }
    }
};
const createUser = {
    method: module_common_1.Http.Method.POST,
    path: '/users',
    options: {
        description: 'Create new user',
        notes: 'All information must valid',
        validate: {
            payload: user_validator_1.createUserRequestValidator
        },
        response: {
            schema: user_validator_1.UserResponseValidator
        },
        tags: ['api', 'users'],
        handler: (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
            const newUser = yield user_service_1.default.createUser(hapiRequest.payload);
            return hapiResponse.response(newUser).code(module_common_1.Http.StatusCode.CREATED);
        }),
        plugins: {
            'hapi-swagger': {
                responses: {
                    [module_common_1.Http.StatusCode.CREATED]: {
                        description: 'User created.'
                    }
                }
            }
        }
    }
};
const userController = [getUser, createUser];
exports.default = userController;
//# sourceMappingURL=user.controller.js.map