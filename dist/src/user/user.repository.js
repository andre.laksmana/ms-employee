"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_model_1 = require("./user.model");
const userDocumentToObject = (document) => document.toObject({ getters: true });
const userDocumentsToObjects = (documents) => documents.map(userDocumentToObject);
const get = () => __awaiter(void 0, void 0, void 0, function* () {
    const documents = yield user_model_1.UserModel.find().exec();
    return userDocumentsToObjects(documents);
});
const create = (user) => __awaiter(void 0, void 0, void 0, function* () {
    const newUser = new user_model_1.UserModel(user);
    yield newUser.save();
    return userDocumentToObject(newUser);
});
const getFirstByName = (name) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield user_model_1.UserModel.findOne({ name }).exec();
    return user && userDocumentToObject(user);
});
const userRepository = {
    get,
    create,
    getFirstByName
};
exports.default = userRepository;
//# sourceMappingURL=user.repository.js.map