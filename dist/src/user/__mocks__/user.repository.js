"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    get: jest.fn(),
    create: jest.fn(),
    getFirstByName: jest.fn()
};
//# sourceMappingURL=user.repository.js.map