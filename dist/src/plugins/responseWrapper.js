"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cls_hooked_1 = require("cls-hooked");
const logger_1 = __importDefault(require("../logger"));
const HttpResponse_1 = require("../common/HttpResponse");
const AppError_1 = require("../errors/AppError");
const constant_1 = require("../common/constant");
const documentPathRegex = /^\/(documentation|swagger)/;
const handleHapiResponse = (hapiRequest, hapiResponse) => {
    // ignore document ui path
    if (documentPathRegex.test(hapiRequest.url.pathname))
        return hapiResponse.continue;
    const httpResponse = new HttpResponse_1.HttpResponse();
    const responseData = hapiResponse.request.response;
    if (responseData instanceof Error) {
        // parse raw error not coming from server handler, ex: joi validation
        if (!responseData.isServer) {
            httpResponse.fail({
                message: responseData.output.payload.message,
                code: responseData.output.payload.error
            }, responseData.output.statusCode);
        }
        logger_1.default.error(responseData.message, responseData);
        if (responseData instanceof AppError_1.AppError) {
            const errors = responseData.getErrors();
            httpResponse.fail({
                message: errors.message,
                code: responseData.errorCode,
                errors: errors.errors
            }, errors.statusCode);
        }
        else {
            httpResponse.fail({
                message: responseData.output.payload.message,
                code: responseData.output.payload.error
            }, responseData.output.statusCode);
        }
    }
    else {
        httpResponse.success(responseData.source, responseData.statusCode);
    }
    let response = hapiResponse
        .response(httpResponse.getBody())
        .code(httpResponse.getStatusCode());
    if (cls_hooked_1.getNamespace(constant_1.Tracing.TRACER_SESSION)) {
        const transactionId = cls_hooked_1.getNamespace(constant_1.Tracing.TRACER_SESSION).get(constant_1.Tracing.TRANSACTION_ID);
        response.header(constant_1.Tracing.TRANSACTION_ID, transactionId);
    }
    return response;
};
const responseWrapper = {
    name: 'responseWrapper',
    version: '1.0.0',
    register: (server) => {
        server.ext('onPreResponse', handleHapiResponse);
    },
    once: true
};
exports.default = responseWrapper;
//# sourceMappingURL=responseWrapper.js.map