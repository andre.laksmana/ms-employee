"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cls_hooked_1 = require("cls-hooked");
const constant_1 = require("../common/constant");
const uuidv4 = require('uuid/v4');
const session = cls_hooked_1.createNamespace(constant_1.Tracing.TRACER_SESSION);
const handleHapiRequest = (hapiRequest, hapiResponse) => __awaiter(void 0, void 0, void 0, function* () {
    const transactionId = hapiRequest.headers[constant_1.Tracing.TRANSACTION_ID] || uuidv4();
    session.bindEmitter(hapiRequest.raw.req);
    session.bindEmitter(hapiRequest.raw.res);
    const clsCtx = session.createContext();
    session.enter(clsCtx);
    // @ts-ignore better approach?
    hapiRequest.app[constant_1.Tracing.TRACER_SESSION] = {
        context: clsCtx
    };
    session.set(constant_1.Tracing.TRANSACTION_ID, transactionId);
    return hapiResponse.continue;
});
const requestWrapper = {
    name: 'requestWrapper',
    version: '1.0.0',
    register: (server) => {
        server.ext('onRequest', handleHapiRequest);
    },
    once: true
};
exports.default = requestWrapper;
//# sourceMappingURL=requestWrapper.js.map