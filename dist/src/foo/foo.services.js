"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const foo_repositories_1 = require("./foo.repositories");
const getFoos = () => __awaiter(void 0, void 0, void 0, function* () {
    const entities = yield foo_repositories_1.fooRepository.getFoos();
    return entities.map(entity => {
        const value = {
            name: entity.name
        };
        return value;
    });
});
const fooService = {
    getFoos
};
exports.fooService = fooService;
//# sourceMappingURL=foo.services.js.map