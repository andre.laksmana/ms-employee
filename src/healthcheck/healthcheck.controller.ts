import hapi from '@hapi/hapi';

const ping: hapi.ServerRoute = {
  method: 'GET',
  path: '/ping',
  options: {
    description: 'Pongs back',
    notes: 'To check is service pongs on a ping',
    tags: ['api'],
    handler: (_request: hapi.Request, _h: hapi.ResponseToolkit) => {
      return 'pong!';
    }
  }
};
const healthController: hapi.ServerRoute[] = [ping];
export default healthController;
