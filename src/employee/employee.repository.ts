import { EmployeeModel, EmployeeDocument } from './employee.model';
import { IEmployee, IEmployeeResponse } from './employee.interface';
import { createSetInstructions } from '../common/mongoDbUtil';

const employeeDocumentToObject = (document: EmployeeDocument) =>
  document.toObject({ getters: true }) as IEmployeeResponse;

const employeeDocumentsToObjects = (documents: EmployeeDocument[]) =>
  documents.map(employeeDocumentToObject);

const get = async (): Promise<IEmployee[]> => {
  const documents = await EmployeeModel.find().exec();
  // if (documents.length === 0) {
  //   return [];
  // }
  return employeeDocumentsToObjects(documents);
};

const create = async (employee: IEmployee): Promise<IEmployee> => {
  const newEmployee = new EmployeeModel(employee);

  await newEmployee.save();

  return employeeDocumentToObject(newEmployee);
};

const updateByEmployeeId = async (
  employeeId: string,
  updateEmployeeInput: IEmployee
): Promise<IEmployee | null> => {
  const employee = await EmployeeModel.findOneAndUpdate(
    { _id: employeeId },
    { $set: createSetInstructions(updateEmployeeInput) },
    { new: true }
  );
  return employee && employeeDocumentToObject(employee);
};

const deleteByEmployeeId = async (employeeId: string): Promise<boolean> => {
  const result = await EmployeeModel.deleteOne({
    _id: employeeId
  });
  return !!result.n;
};

const getFirstByName = async (name: string) => {
  const employee = await EmployeeModel.findOne({ name }).exec();
  return employee && employeeDocumentToObject(employee);
};

const employeeRepository = {
  get,
  create,
  getFirstByName,
  updateByEmployeeId,
  deleteByEmployeeId
};
export default employeeRepository;
