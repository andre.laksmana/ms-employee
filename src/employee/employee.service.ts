import { ERROR_CODE } from '../common/errors';

import employeeRepository from './employee.repository';
import { IEmployee } from './employee.interface';
import { AppError } from '../errors/AppError';

const getEmployees = () => {
  return employeeRepository.get();
};

const createEmployee = async (employee: IEmployee) => {
  const existingEmployee = await employeeRepository.getFirstByName(
    employee.name
  );
  if (existingEmployee) {
    throw new AppError(ERROR_CODE.USER_NAME_EXISTED);
  }
  return employeeRepository.create(employee);
};

const updateEmployee = async (employee: IEmployee) => {
  const existingEmployee = await employeeRepository.getFirstByName(
    employee.name
  );

  if (existingEmployee) {
    return employeeRepository.updateByEmployeeId(existingEmployee.id, employee);
  } else {
    throw new AppError(ERROR_CODE.USER_NAME_NOT_EXISTED);
  }
};

const deleteEmployee = async (employee: IEmployee) => {
  const existingEmployee = await employeeRepository.getFirstByName(
    employee.name
  );

  if (existingEmployee) {
    return employeeRepository.deleteByEmployeeId(existingEmployee.id);
  } else {
    throw new AppError(ERROR_CODE.USER_NAME_NOT_EXISTED);
  }
};

const employeeService = {
  getEmployees,
  createEmployee,
  updateEmployee,
  deleteEmployee
};
export default employeeService;
