import * as Joi from '@hapi/joi';

import employeeConstant from './employee.constant';

import { MongooseBase } from '../common/validators';

const EmployeeValidator = {
  name: Joi.string()
    .trim()
    .required(),
  age: Joi.number()
    .min(employeeConstant.MIN_USER_AGE)
    .required(),
  mobile: Joi.string().trim()
};

const EmployeeResponseValidator = Joi.object({
  ...MongooseBase,
  ...EmployeeValidator
})
  .required()
  .label('Response - Employee');

const EmployeeListResponseValidator = Joi.array()
  .items(EmployeeResponseValidator)
  .label('Response - Employees');

const createEmployeeRequestValidator = Joi.object({
  ...EmployeeValidator
}).label('Request - new employee');

const updateEmployeeRequestValidator = Joi.object({
  ...EmployeeValidator
}).label('Request - update employee');

const deleteEmployeeRequestValidator = Joi.object({
  ...EmployeeValidator
}).label('Request - delete employee');

export {
  EmployeeResponseValidator,
  createEmployeeRequestValidator,
  updateEmployeeRequestValidator,
  deleteEmployeeRequestValidator,
  EmployeeListResponseValidator,
  EmployeeValidator
};
