import hapi = require('@hapi/hapi');

import employeeService from '../employee.service';
import employeeController from '../employee.controller';
import { IEmployee } from '../employee.interface';
jest.mock('../employee.service', () => ({
  getEmployees: jest.fn(),
  createEmployee: jest.fn(),
  updateEmployee: jest.fn(),
  deleteEmployee: jest.fn()
}));
let server: hapi.Server;

describe('employee.controller', () => {
  beforeAll(async () => {
    server = new hapi.Server();
    server.route(employeeController);
  });

  describe('GET /employees', () => {
    it('should return employeeService getEmployees with code 200', async () => {
      const testEmployees = [
        {
          _id: '5d29b6716394dea3588023d4',
          name: 'mahesh',
          age: 20,
          __v: 0,
          id: '5d29b6716394dea3588023d4'
        }
      ];
      employeeService.getEmployees.mockResolvedValueOnce(testEmployees);
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'GET',
        url: `/employees`
      });
      expect(result.statusCode).toBe(200);
      expect(result.result).toEqual(testEmployees);
      expect(employeeService.getEmployees).toHaveBeenCalledTimes(1);
    });
  });

  describe('POST /employees', () => {
    it('should return error on empty name', async () => {
      const testEmployee: IEmployee = {
        name: '',
        age: 20
      };
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(result.statusCode).toBe(400);
      expect(result.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return error on not enough age', async () => {
      const testEmployee: IEmployee = {
        name: 'test',
        age: 10
      };
      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(response.statusCode).toBe(400);
      expect(response.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return 201 on valid payload and success service call', async () => {
      const testEmployee: IEmployee = {
        name: 'test',
        age: 18
      };

      employeeService.createEmployee.mockResolvedValueOnce({
        ...testEmployee,
        id: 'new id'
      });

      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(response.statusCode).toBe(201);
      expect(response.result).toEqual({
        ...testEmployee,
        id: 'new id'
      });
    });
  });

  describe('PUT /employees', () => {
    it('should return error on empty name', async () => {
      const testEmployee: IEmployee = {
        name: '',
        age: 20
      };
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'PUT',
        url: `/employees`,
        payload: testEmployee
      });

      expect(result.statusCode).toBe(400);
      expect(result.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return error on not enough age', async () => {
      const testEmployee: IEmployee = {
        name: 'test',
        age: 10
      };
      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'PUT',
        url: `/employees`,
        payload: testEmployee
      });

      expect(response.statusCode).toBe(400);
      expect(response.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return 200 on valid payload and success service call', async () => {
      const modEmployee: IEmployee = {
        name: 'test',
        age: 20
      };

      employeeService.updateEmployee.mockResolvedValueOnce({
        ...modEmployee,
        id: 'new id'
      });

      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'PUT',
        url: `/employees`,
        payload: modEmployee
      });

      expect(response.statusCode).toBe(200);
      expect(response.result).toEqual({
        ...modEmployee,
        id: 'new id'
      });
    });
  });
});
