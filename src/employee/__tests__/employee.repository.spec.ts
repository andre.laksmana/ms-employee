import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';

import { EmployeeModel } from '../employee.model';
import employeeRepository from '../employee.repository';

jest.mock('mongoose', () => {
  const mongoose = require.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

describe('employee.repository', () => {
  let mongod: MongoMemoryServer;
  beforeAll(async () => {
    mongod = new MongoMemoryServer();
    const mongoDbUri = await mongod.getConnectionString();
    await mongoose.connect(mongoDbUri, { useNewUrlParser: true });
  });

  afterAll(async () => {
    mongoose.disconnect();
    mongod.stop();
  });

  describe('get', () => {
    it('should get all employees', async () => {
      await EmployeeModel.create(
        {
          name: 'u1'
        },
        {
          name: 'u2'
        }
      );

      const employees = await employeeRepository.get();
      expect(employees).toHaveLength(2);
    });
  });

  describe('create', () => {
    it('should create new employee', async () => {
      const newEmployee = await employeeRepository.create({
        name: 'new employee',
        age: 10
      });
      expect(newEmployee.id).toBeDefined();
    });
  });

  describe('getFirstByName', () => {
    it('should return first employee if name exists in db', async () => {
      await EmployeeModel.create({
        name: 'exist name'
      });
      const employee = await employeeRepository.getFirstByName('exist name');
      expect(employee.name).toEqual('exist name');
    });

    it('should return null if name not exists in db', async () => {
      const employee = await employeeRepository.getFirstByName(
        'not exist name'
      );
      expect(employee).toBeNull();
    });
  });

  describe('update', () => {
    it('should update employee', async () => {
      const newEmployee = await employeeRepository.create({
        name: 'new employee',
        age: 20
      });
      const newEmployeeObj = await employeeRepository.getFirstByName(
        newEmployee.name
      );

      const modEmployee = {
        name: 'new employee',
        age: 30
      };

      const modifiedEmployee = await employeeRepository.updateByEmployeeId(
        newEmployeeObj.id,
        modEmployee
      );

      expect(modifiedEmployee.age).toEqual(30);
    });
  });

  describe('delete', () => {
    it('should delete employee', async () => {
      const newEmployee = await employeeRepository.create({
        name: 'new employee',
        age: 20
      });
      const newEmployeeObj = await employeeRepository.getFirstByName(
        newEmployee.name
      );

      const isDeleted = await employeeRepository.deleteByEmployeeId(
        newEmployeeObj.id
      );

      expect(isDeleted).toBe(true);
    });
  });
});
