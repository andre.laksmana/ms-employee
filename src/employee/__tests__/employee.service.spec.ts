import employeeService from '../employee.service';
import employeeRepository from '../employee.repository';
import { ERROR_CODE } from '../../common/errors';
import { AppError } from '../../errors/AppError';

jest.mock('../employee.repository');

describe('employeeService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('getEmployees', () => {
    it('should return repository get result', async () => {
      const employeesTest = [
        {
          name: 'test'
        }
      ];
      employeeRepository.get.mockResolvedValueOnce(employeesTest);
      const employees = await employeeService.getEmployees();
      expect(employees).toEqual(employeesTest);
    });
  });

  describe('createEmployee', () => {
    it('should throw error if repository find existing employee with same name', async () => {
      const employeeInfo = {
        name: 'test',
        age: 12
      };
      employeeRepository.getFirstByName.mockResolvedValueOnce({
        name: 'test'
      });
      try {
        await employeeService.createEmployee(employeeInfo);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect(e.errorCode).toEqual(ERROR_CODE.USER_NAME_EXISTED);
      } finally {
        expect(employeeRepository.create).not.toBeCalled();
      }
    });

    it('should call repository create employee', async () => {
      const employeeInfo = {
        name: 'test',
        age: 12
      };
      employeeRepository.getFirstByName.mockResolvedValueOnce(null);
      await employeeService.createEmployee(employeeInfo);
      expect(employeeRepository.create).toBeCalledWith(employeeInfo);
    });
  });

  describe('updateEmployee', () => {
    it('should throw error if repository can not find existing employee with same name', async () => {
      const employeeInfo = {
        name: 'deliver',
        age: 22
      };
      employeeRepository.getFirstByName.mockResolvedValueOnce(null);
      try {
        await employeeService.updateEmployee(employeeInfo);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect(e.errorCode).toEqual(ERROR_CODE.USER_NAME_NOT_EXISTED);
      } finally {
        expect(employeeRepository.updateByEmployeeId).not.toBeCalled();
      }
    });

    it('should call repository update employee', async () => {
      const employeeInfo = {
        name: 'test',
        age: 22
      };
      const existingEmployee = employeeRepository.getFirstByName.mockResolvedValueOnce(
        {
          name: 'test',
          age: 32
        }
      );
      await employeeService.updateEmployee(employeeInfo);
      expect(employeeRepository.updateByEmployeeId).toBeCalledWith(
        existingEmployee.id,
        employeeInfo
      );
    });
  });

  describe('deleteEmployee', () => {
    it('should throw error if repository can not find existing employee with same name', async () => {
      const employeeInfo = {
        name: 'delete',
        age: 22
      };
      employeeRepository.getFirstByName.mockResolvedValueOnce(null);
      try {
        await employeeService.deleteEmployee(employeeInfo);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect(e.errorCode).toEqual(ERROR_CODE.USER_NAME_NOT_EXISTED);
      } finally {
        expect(employeeRepository.deleteByEmployeeId).not.toBeCalled();
      }
    });

    it('should call repository delete employee', async () => {
      const employeeInfo = {
        name: 'test',
        age: 32
      };
      const existingEmployee = employeeRepository.getFirstByName.mockResolvedValueOnce(
        {
          name: 'test',
          age: 32
        }
      );
      await employeeService.deleteEmployee(employeeInfo);
      expect(employeeRepository.deleteByEmployeeId).toBeCalledWith(
        existingEmployee.id
      );
    });
  });
});
