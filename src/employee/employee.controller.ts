import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import employeeService from './employee.service';
import {
  EmployeeListResponseValidator,
  createEmployeeRequestValidator,
  updateEmployeeRequestValidator,
  deleteEmployeeRequestValidator,
  EmployeeResponseValidator
} from './employee.validator';
import { IEmployeeRequest } from './employee.interface';

const getEmployee: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/employees',
  options: {
    description: 'Get the list of employees',
    notes: 'Get the list of employees',
    tags: ['api', 'employees'],
    response: {
      schema: EmployeeListResponseValidator
    },
    handler: async () => {
      const employeeList = await employeeService.getEmployees();
      return employeeList;
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employees retrieved'
          }
        }
      }
    }
  }
};
const createEmployee: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/employees',
  options: {
    description: 'Create new employee',
    notes: 'All information must valid',
    validate: {
      payload: createEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newEmployee = await employeeService.createEmployee(
        hapiRequest.payload
      );
      return hapiResponse.response(newEmployee).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee created.'
          }
        }
      }
    }
  }
};
const updateEmployee: hapi.ServerRoute = {
  method: Http.Method.PUT,
  path: '/employees',
  options: {
    description: 'Update existing employee',
    notes: 'Employee must exist and all information must valid',
    validate: {
      payload: updateEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const updatedEmployee = await employeeService.updateEmployee(
        hapiRequest.payload
      );
      if (updatedEmployee) {
        return hapiResponse
          .response(updatedEmployee)
          .code(Http.StatusCode.MODIFIED);
      } else {
        return hapiResponse
          .response()
          .code(Http.StatusCode.INTERNAL_SERVER_ERROR);
      }
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employee updated.'
          }
        }
      }
    }
  }
};
const deleteEmployee: hapi.ServerRoute = {
  method: Http.Method.DELETE,
  path: '/employees',
  options: {
    description: 'Delete existing employee',
    notes: 'Employee must exist',
    validate: {
      payload: deleteEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const isEmployeeDeleted = await employeeService.deleteEmployee(
        hapiRequest.payload
      );
      if (isEmployeeDeleted) {
        return hapiResponse.response().code(Http.StatusCode.DELETED);
      } else {
        return hapiResponse
          .response()
          .code(Http.StatusCode.INTERNAL_SERVER_ERROR);
      }
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employee updated.'
          }
        }
      }
    }
  }
};

const employeeController: hapi.ServerRoute[] = [
  getEmployee,
  createEmployee,
  updateEmployee,
  deleteEmployee
];
export default employeeController;
