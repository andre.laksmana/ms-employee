export default {
  get: jest.fn(),
  create: jest.fn(),
  getFirstByName: jest.fn(),
  updateByEmployeeId: jest.fn(),
  deleteByEmployeeId: jest.fn()
};
