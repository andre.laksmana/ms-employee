import { connect, connection } from 'mongoose';
import { config } from '@dk/module-config';
import logger from '../logger';
import { Util } from '@dk/module-common';

// TODO: use object to wrap mongo configuration in env
export const MONGO_USER_PARAM = 'MONGO_USER';
export const MONGO_PASS_PARAM = 'MONGO_PASS';
export const MONGO_DB_NAME = 'MONGO_DB_NAME';
export const MONGO_HOSTS = 'MONGO_HOSTS';
export const MONGO_RECONNECT_RETRIES = 'MONGO_RECONNECT_RETRIES';
export const MONGO_RECONNECT_INTERVAL = 'MONGO_RECONNECT_INTERVAL';

export const connectMongo = () =>
  new Promise<void>((resolve, reject) => {
    const user = config.get(MONGO_USER_PARAM);
    const pass = process.env.MONGO_PASSWORD || '';
    const dbName = config.get(MONGO_DB_NAME);
    const dbHosts = config.get(MONGO_HOSTS);
    const reconnectInterval = Number(config.get(MONGO_RECONNECT_INTERVAL)) || 0;
    const reconnectTries = Number(config.get(MONGO_RECONNECT_RETRIES)) || 3;
    const autoReconnect = reconnectInterval > 0;

    let dbUri = '';
    if (user) {
      dbUri = Util.getMongoUri(user, pass, dbName, dbHosts);
    } else {
      dbUri = 'mongodb://' + dbHosts + '/' + dbName;
    }

    // const dbUri =
    //   'mongodb+srv://andre:spLJkFUF7SlOWjv4@cluster0-ufejz.mongodb.net/test?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=true';

    if (!dbName) {
      logger.error(`No ${MONGO_DB_NAME} is provided`);
      return reject(`No ${MONGO_DB_NAME} is provided`);
    }

    connection.on('error', (err: any) => {
      logger.error('error while connecting to mongodb', err);
    });

    if (!autoReconnect) {
      connection.once('error', reject); // reject first error
    }

    connection.once('open', () => {
      if (!autoReconnect) {
        connection.off('error', reject);
      }
      resolve();
    });

    connection.on('reconnected', () => {
      logger.info('Connection to mongodb is resumed');
    });

    connection.on('disconnected', () => {
      logger.error('Mongodb disconnected');
    });

    connect(
      dbUri,
      {
        useNewUrlParser: true,
        user,
        pass,
        dbName,
        autoReconnect,
        reconnectTries,
        reconnectInterval
      }
    );
  });
