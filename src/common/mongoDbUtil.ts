export const createSetInstructions = (
  obj: Record<string, any>,
  rootPath?: string
): { [key: string]: any } => {
  const path = rootPath ? `${rootPath}.` : '';
  return Object.keys(obj).reduce((map: { [key: string]: any }, key) => {
    const value = obj[key];
    if (value !== undefined) {
      map[`${path}${key}`] = obj[key];
      return map;
    }
    return map;
  }, {});
};
