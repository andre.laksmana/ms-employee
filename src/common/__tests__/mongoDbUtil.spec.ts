import { createSetInstructions } from '../mongoDbUtil';

interface Sample {
  firstName?: string;
  lastName?: string;
}

describe('mongoDbUtil', () => {
  it('should flat object to $set instructions', () => {
    const sample: Sample = {
      firstName: 'foo',
      lastName: 'bar'
    };

    const instructions = createSetInstructions(sample);

    expect(instructions['firstName']).toBe('foo');
    expect(instructions['lastName']).toBe('bar');
  });

  it('should flat object to $set instructions with appended root path', () => {
    const sample: Sample = {
      lastName: 'bar'
    };

    const instructions = createSetInstructions(sample, 'user');

    expect(instructions['user.lastName']).toBe('bar');
  });

  it('should remove undefined value in flatten instruction', () => {
    const sample: Sample = {
      lastName: 'bar',
      firstName: undefined
    };

    const instructions = createSetInstructions(sample);

    expect(instructions['firstName']).toBeUndefined();
  });
});
