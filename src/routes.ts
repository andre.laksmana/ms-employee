import healthcheck from './healthcheck/healthcheck.controller';
import employeeController from './employee/employee.controller';

const routes = [...healthcheck, ...employeeController];

export { routes };
